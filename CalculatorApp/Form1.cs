﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Forms;
using System.IO;

namespace CalculatorApp
{
    public partial class Form1 : Form
    {
        private double resultValue = 0;
        private String operationPerfom = "";
        private bool isOperationPerformed = false;
        private bool isEqualsPerformed = false;
        
        public Form1()
        {
            InitializeComponent();
        }

       
        private void Button_Click(object sender, EventArgs e)
        {
            if((TextBox_Result.Text =="0") || (isOperationPerformed))
                TextBox_Result.Clear();

            if (isEqualsPerformed)
            {
                TextBox_Result.Clear();
                isEqualsPerformed = false;
            }
                


            isOperationPerformed = false;
            Button button = (Button) sender;
            if (button.Text == ".")
            {
                if (!TextBox_Result.Text.Contains("."))
                {
                    TextBox_Result.Text = TextBox_Result.Text + button.Text;
                }
            }
            else
            TextBox_Result.Text = TextBox_Result.Text + button.Text;

            
        }

        private void operator_Click_Event(object sender, EventArgs e)
        {
            
            Button button = (Button) sender;

            //If +, -, * or / are clicked twice in a row.
            //Nullify the existing calculation and clear the calculation text.
            if (isOperationPerformed) 
            {
                CurrentCalculation.Text = "";
                resultValue = 0;
                isOperationPerformed = false;
            }

            //If the result of the calculation is not 0 and the equals button has not been pressed prior.
            //update the calculation, then add the operator to the visible calculation.
            if (resultValue != 0 && !isEqualsPerformed)
                {
                    PerformCalculation(sender, e);
                    operationPerfom = button.Text;
                    resultValue = Double.Parse(TextBox_Result.Text);
                    CurrentCalculation.Text = CurrentCalculation.Text + " " + operationPerfom;
                    isOperationPerformed = true;
                }

            //If the result of the calculation is not 0 and the Equals key was the previous button pressed.
            //The result in the Text box becomes the current calculation value and the operator is added to the visible calculation.
                if (resultValue != 0 && isEqualsPerformed)
                {
                    CurrentCalculation.Text = TextBox_Result.Text;
                    operationPerfom = button.Text;
                    resultValue = Double.Parse(TextBox_Result.Text);
                    CurrentCalculation.Text = CurrentCalculation.Text + " " + operationPerfom;
                    isOperationPerformed = true;
                    isEqualsPerformed = false;
                }

                //if the result of the calculation is 0, the equals key was not the last key pressed AND the text box does not equal 0,
                // Set the Result value to the value in the Text box.
                if (resultValue == 0 && !isEqualsPerformed && TextBox_Result.Text != "0")
                {
                    operationPerfom = button.Text;
                    resultValue = Double.Parse(TextBox_Result.Text, NumberStyles.Any, CultureInfo.InvariantCulture);

                    //if the result of the calculation is 0 and the Text box equals 0,
                    // update the calculation text box to 0.
                    if (resultValue == 0 && TextBox_Result.Text == "0")
                    {
                        CurrentCalculation.Text = "0";
                    }
                    else
                    {
                        CurrentCalculation.Text = CurrentCalculation.Text + " " + TextBox_Result.Text + " " + operationPerfom;
                    }
                    

                    isOperationPerformed = true;
                }


            if (CurrentCalculation.Text == "")
                CurrentCalculation.Text = "";


        }

        private void ClearEntry(object sender, EventArgs e)
        {
            TextBox_Result.Text = "0";
        }

        private void ClearAll(object sender, EventArgs e)
        {
            TextBox_Result.Text = "0";
            resultValue = 0;
            CurrentCalculation.Text = "";

        }

        private void PerformCalculation(object sender, EventArgs e)
        {
            CurrentCalculation.Text = CurrentCalculation.Text + " " + TextBox_Result.Text;
            switch (operationPerfom)
            {
                case "+":
                    TextBox_Result.Text = (resultValue + Double.Parse(TextBox_Result.Text)).ToString();
                    break;

                case "-":
                    TextBox_Result.Text = (resultValue - Double.Parse(TextBox_Result.Text)).ToString();
                    break;

                case "/":
                    TextBox_Result.Text = (resultValue / Double.Parse(TextBox_Result.Text)).ToString();
                    break;

                case "*":
                    TextBox_Result.Text = (resultValue * Double.Parse(TextBox_Result.Text)).ToString();
                    break;
                default:
                    break;
            }

        }

        private void EqualsButton(object sender, EventArgs e)
        {
            if (isEqualsPerformed || CurrentCalculation.Text == null || CurrentCalculation.Text == "")
            {
                CurrentCalculation.Text = "";
            }
            if (isEqualsPerformed && CurrentCalculation.Text != "")
            {
                CurrentCalculation.Text = resultValue.ToString();
                operationPerfom = "";
            }
           if(!isEqualsPerformed && TextBox_Result.Text != "")
            {
                isEqualsPerformed = true;
                PerformCalculation(sender, e);

                resultValue = Double.Parse(TextBox_Result.Text);
                CurrentCalculation.Text = resultValue.ToString();
                operationPerfom = "";
                using (StreamWriter file = new StreamWriter(@"C:\Users\Davidc\source\repos\CalculatorApp\Calc_logs\CalcLog.txt",true))
                {
                   file.WriteLine("Calculation = " + CurrentCalculation.Text);
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
